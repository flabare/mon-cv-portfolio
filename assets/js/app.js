/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import '../scss/app.scss';

// start the Stimulus application
import '../bootstrap';
import $ from 'jquery';
import 'popper.js';
import 'bootstrap';

$("body").on("click", ".collection-add", e => {
   
    let collection =  $(e.currentTarget).data('collection');
    let collectionContainer =  $('#' + collection);
    console.log(collectionContainer)
    let prototype = collectionContainer.data('prototype');
    let index = collectionContainer.data('index');

    collectionContainer.append(prototype.replace(/__name__/g, index));
    collectionContainer.data('index', index ++);
})
