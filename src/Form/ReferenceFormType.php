<?php

namespace App\Form;

use App\Entity\Reference;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReferenceFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jobTitle', TextType::class, [
                'label' => 'Intitulé du poste',
                'attr' => [
                    'placeholder' => 'Entrez l\'intitulé du poste ...'
                ]
            ])
            ->add('compagny', TextType::class, [
                'label' => 'Nom de l\'entreprise',
                'attr' => [
                    'placeholder' => 'Entrez le nom de l\'entreprise...'
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description du poste',
                'attr' => [
                    'placeholder' => 'Entrez la description de votre expérience ...'
                ]
            ])
            ->add('startedAt', DateType::class, [
                'label' => 'Date d\'entrée dans le poste',
                'input' => 'datetime_immutable',
                'widget' => 'single_text'
            ])
            ->add('endedAt', DateType::class, [
                'label' => 'Date de fin',
                'input' => 'datetime_immutable',
                'widget' => 'single_text',
                'required' => false
            ])
            ->add('medias', CollectionType::class, [
                'entry_type' => MediaFormType::class,
                'allow_add'=> true,
                'allow_delete'=> true,
                'by_reference' => false,
                'entry_options' => ['label' => false]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data-class', Reference::class);
    }
}
