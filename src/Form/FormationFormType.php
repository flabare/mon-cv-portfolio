<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Formation;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType as TypeDateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class FormationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom de la formation',
                'attr' => [
                    'placeholder' => 'Entrez le nom de la formation'
                ]
            ])
            ->add('school', TextType::class, [
                'label' => 'Nom de l\'école ou de l\'organisme',
                'attr' => [
                    'placeholder' => 'Entrez le nom de l\'école...'
                ]
            ])
            ->add("gradeLevel", ChoiceType::class, [
                'label' => 'Niveau d\'étude',
                'choices' => Formation::GRADE_LEVELS
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description de la formation'
            ])
            ->add('startedAt', TypeDateType::class, [
                'label' => 'Début de la formation',
                'input' => 'datetime_immutable',
                'widget' => 'single_text'
            ])
            ->add('endedAt', TypeDateType::class, [
                'label' => 'Fin de la formation',
                'input' => 'datetime_immutable',
                'widget' => 'single_text',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data-class', Formation::class);
    }
}
