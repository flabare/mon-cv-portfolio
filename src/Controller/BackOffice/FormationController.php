<?php

namespace App\Controller\BackOffice;

use App\Form\FormationFormType;
use App\Entity\Formation;
use App\Repository\FormationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/formations")
 */
class FormationController extends AbstractController
{
    /**
     * @Route(name="formations_list")
     * @param FormationRepository $formationRepository
     * @return Response
     */
    public function formationsList(FormationRepository $formationRepository): Response
    {
        $formations = $formationRepository->findAll();

        return $this->render('back-office/formation/formations-list.html.twig', [
            'formations' => $formations,
        ]);
    }

    /**
     * @Route("/create", name="formation_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $formation = new Formation();
    
        $form = $this->createForm(FormationFormType::class, $formation)->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($formation);
            $entityManager->flush();
            $this->addFlash('success', 'la formation a été ajouée avec succès !');

            return $this->redirectToRoute('formations_list');
        }

        return $this->render('back-office/formation/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/update", name="formation_update")
     * @param Formation $formation
     * @param Request $request
     * @return Response
     */
    public function update(Formation $formation, Request $request): Response
    {
        $form = $this->createForm(FormationFormType::class, $formation)->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('success', 'la formation a été modifiée avec succès !');

            return $this->redirectToRoute('formations_list');
        }

        return $this->render('back-office/formation/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="formation_delete")
     * @param Formation $formation
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Formation $formation): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($formation);
        $entityManager->flush();
        $this->addFlash('success', 'la formation a été supprimée avec succès !');

        return $this->redirectToRoute('formations_list');
    }
    
}
