<?php

namespace App\Controller\BackOffice;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ReferenceRepository;
use App\Entity\Reference;
use App\Form\ReferenceFormType;

/**
 * @Route("/admin/references")
 */
class ReferenceController extends AbstractController
{
   /**
     * @Route(name="references_list")
     * @param ReferenceRepository $referenceRepository
     * @return Response
     */
    public function referencesList(ReferenceRepository $referenceRepository): Response
    {
        $references = $referenceRepository->findAll();

        return $this->render('back-office/reference/references-list.html.twig', [
            'references' => $references,
        ]);
    }

    /**
     * @Route("/create", name="reference_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $reference = new Reference();
    
        $form = $this->createForm(ReferenceFormType::class, $reference)->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($reference);
            $entityManager->flush();
            $this->addFlash('success', 'la référence a été ajouée avec succès !');

            return $this->redirectToRoute('references_list');
        }

        return $this->render('back-office/reference/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/update", name="reference_update")
     * @param Reference $reference
     * @param Request $request
     * @return Response
     */
    public function update(Reference $reference, Request $request): Response
    {
        $form = $this->createForm(ReferenceFormType::class, $reference)->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('success', 'la référence a été modifiée avec succès !');

            return $this->redirectToRoute('references_list');
        }

        return $this->render('back-office/reference/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="reference_delete")
     * @param Reference $reference
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Reference $reference): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($reference);
        $entityManager->flush();
        $this->addFlash('success', 'la référence a été supprimée avec succès !');

        return $this->redirectToRoute('references_list');
    }
}
