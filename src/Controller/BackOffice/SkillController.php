<?php

namespace App\Controller\BackOffice;

use App\Form\SkillFormType;
use App\Repository\SkillRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Skill;

/**
 * @Route("/admin/skills")
*/
class SkillController extends AbstractController
{
    /**
     * @Route(name="skills_list")
     * @param SkillRepository $skillRepository
     * @return Response
     */
    public function skillsList(SkillRepository $skillRepository): Response 
    {
        $skills = $skillRepository->findAll();

        return $this->render('back-office/skill/skills-list.html.twig', [
            'skills' => $skills 
        ]);
    }

    /**
     * @Route("/create", name="skill_create")
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $skill = new Skill();

        $form = $this->createForm(SkillFormType::class, $skill)->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($skill);
            $entityManager->flush();
            $this->addFlash('success', 'la compétences a été ajouée avec succès !');

            return $this->redirectToRoute('skills_list');
        }

        return $this->render('back-office/skill/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/update", name="skill_update")
     * @param Skill $skill
     * @param Request $request
     * @return Response
     */
    public function update(Skill $skill, Request $request): Response
    {
        $form = $this->createForm(SkillFormType::class, $skill)->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            $this->addFlash('success', 'la compétences a été modifiée avec succès !');

            return $this->redirectToRoute('skills_list');
        }

        return $this->render('back-office/skill/update.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    /**
     * @Route("{id}/delete", name="skill_delete")
     * @param Skill $skill
     * @return RedirectResponse
     */
    public function delete(Skill $skill): RedirectResponse
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($skill);
        $entityManager->flush();
        $this->addFlash('success', 'la compétences a été supprimée avec succès !');

        return $this->redirectToRoute('skills_list');
    }
}
