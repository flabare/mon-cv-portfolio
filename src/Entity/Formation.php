<?php

namespace App\Entity;

use App\Repository\FormationRepository;
use Doctrine\ORM\Mapping as ORM;
use DateTimeInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=FormationRepository::class)
 */
class Formation
{
    const BAC = 'BAC' ;
    const BAC_2 = 'BAC + 2/BTS';
    const BAC_3 = 'BAC + 3/License';
    const BAC_4 = 'BAC + 4/Master 1';
    const BAC_5 = 'BAC + 5/Master 2';

    const GRADE_LEVELS = [
        'BAC' => self::BAC,
        'BAC + 2/BTS' => self::BAC_2,
        'BAC + 3/License' => self::BAC_3,
        'BAC + 4/Master 1' => self::BAC_4,
        'BAC + 5/Master 2' => self::BAC_5
    ];

    /** 
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Ce champ ne peut être vide.")
     */
    private ?string $name = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Ce champ ne peut être vide.")
     * @Assert\Choice(choices=Formation::GRADE_LEVELS, message="Veuillez choisir une formation.")
     */
    private ?string $gradeLevel = null;

    /**
     * @var string|null
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Ce champ ne peut être vide.")
     */
    private ?string $description = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Ce champ ne peut être vide.")
     */
    private ?string $school = null;

    /**
     * @var DateTimeInterface|null
     * @ORM\Column(type="date_immutable")
     * @Assert\NotBlank(message="Ce champ ne peut être vide.")
     */
    private ?DateTimeInterface $startedAt =  null;

    /**
     * @var DateTimeInterface|null
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private ?DateTimeInterface $endedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getGradeLevel(): ?string
    {
        return $this->gradeLevel;
    }

    public function setGradeLevel(?string $gradeLevel): self
    {
        $this->gradeLevel = $gradeLevel;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSchool(): ?string
    {
        return $this->school;
    }

    public function setSchool(?string $school): self
    {
        $this->school = $school;

        return $this;
    }

    public function getStartedAt(): ?\DateTimeImmutable
    {
        return $this->startedAt;
    }
    

    public function setStartedAt(\DateTimeImmutable $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getEndedAt(): ?\DateTimeImmutable
    {
        return $this->endedAt;
    }

    public function setEndedAt(?\DateTimeImmutable $endedAt): self
    {
        $this->endedAt = $endedAt;

        return $this;
    }
}
